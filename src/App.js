// Importa las bibliotecas y los componentes necesarios de React y otros archivos locales.
import React, { useState } from "react";

import "./App.css"// Importa el archivo de estilos CSS.
import { Login } from "./pages/Login";
import { Register } from "./pages/Register.jsx";
import { Index } from "./pages/Index.jsx";
import { AdminLogin } from "./pages/AdminLogin.jsx";
import { Administrador } from "./pages/Administrador.jsx";
import { Reservas } from "./pages/Reservas.jsx";
import Habitacion from "./pages/Habitacion.jsx";

// Define el componente principal de la aplicación.
function App() {
  // Utiliza el estado para realizar un seguimiento del formulario actual que se debe mostrar.
  const [currentForm, setCurrentForm] = useState('index');

  // Función para cambiar el formulario actual según el nombre proporcionado.
  const toggleForm = (formName) => {
    setCurrentForm(formName);
  }
   // Un objeto que contiene todos los componentes de formulario disponibles, cada uno asociado con un nombre de formulario.
  const formComponents = {
    index: <Index onFormSwitch={toggleForm} />,
    login: <Login onFormSwitch={toggleForm} />,
    register: <Register onFormSwitch={toggleForm} />,
    adminlogin: <AdminLogin onFormSwitch={toggleForm} />,
    administrador: <Administrador onFormSwitch={toggleForm} />,
    reservas: <Reservas onFormSwitch={toggleForm} />,
    habitacion: <Habitacion onFormSwitch={toggleForm} />,

  };

  // Renderiza el componente actual según el formulario actual en el estado.
  return (
    <div className="App">
      {formComponents[currentForm]}
    </div>
  );
}
// Exporta el componente principal para ser utilizado en otros archivos.
export default App;
