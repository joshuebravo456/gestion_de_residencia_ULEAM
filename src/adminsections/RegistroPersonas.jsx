import React, { useState, useEffect } from 'react';  // Importar React, useState y useEffect desde React

const RegistroPersonas = () => { // Definir el componente funcional RegistroPersonas
  const [personas, setPersonas] = useState(() => {
    const storedPersonas = JSON.parse(localStorage.getItem('userList')) || [];

    // Devolver las personas almacenadas
    return storedPersonas;
  });

  // Definir el estado de 'habitaciones' utilizando useState con un valor inicial obtenido de localStorage o un array vacío
  const [habitaciones, setHabitaciones] = useState(() => {
    // Obtener habitaciones almacenadas en localStorage o un array vacío si no hay datos almacenados
    const storedHabitaciones = JSON.parse(localStorage.getItem('habitaciones')) || [];

    // Devolver las habitaciones almacenadas
    return storedHabitaciones;
  });

  // Definir otros estados usando useState
  const [formularioVisible, setFormularioVisible] = useState(false);
  const [username, setNombre] = useState('');
  const [email, setEmail] = useState('');
  const [telefono, setTelefono] = useState('');
  const [password, setContrasena] = useState('');
  const [habitacion, setHabitacion] = useState('');
  const [rol, setRol] = useState('');
  const [personaEditando, setPersonaEditando] = useState(null);

  // Utilizar useEffect para almacenar las personas en localStorage cada vez que cambia el estado 'personas'
  useEffect(() => {
    localStorage.setItem('userList', JSON.stringify(personas));
  }, [personas]);

  // Utilizar useEffect para almacenar las habitaciones en localStorage cada vez que cambia el estado 'habitaciones'
  useEffect(() => {
    localStorage.setItem('habitaciones', JSON.stringify(habitaciones));
  }, [habitaciones]);

  // Función para manejar la adición de una nueva persona
  const handleAgregarPersona = () => {
    setFormularioVisible(true);
    setPersonaEditando(null);
    setNombre('');
    setEmail('');
    setTelefono('');
    setContrasena('');
    setHabitacion('');
    setRol('');
  };

  const handleGuardarPersona = () => {
    if (!username || !email || !telefono || !password || !habitacion || !rol) {
      alert('Por favor, completa todos los campos.');
      return;
    }

    if (personaEditando) {
      const nuevasPersonas = personas.map((persona) =>
        persona.email === personaEditando.email
          ? { ...persona, username, telefono, password, habitacion, rol }
          : persona
      );
      setPersonas(nuevasPersonas);
    } else {
      const nuevaPersona = {
        username,
        email,
        telefono,
        password,
        habitacion,
        rol,
      };
      setPersonas([...personas, nuevaPersona]);
    }

    setFormularioVisible(false);
    setNombre('');
    setEmail('');
    setTelefono('');
    setContrasena('');
    setHabitacion('');
    setRol('');
    setPersonaEditando(null);
  };

  const handleEditarPersona = (persona) => {
    setFormularioVisible(true);
    setPersonaEditando(persona);
    setNombre(persona.username);
    setEmail(persona.email);
    setTelefono(persona.telefono);
    setContrasena(persona.password);
    setHabitacion(persona.habitacion);
    setRol(persona.rol);
  };

  const handleEliminarPersona = (email) => {
    const nuevasPersonas = personas.filter((persona) => persona.email !== email);
    setPersonas(nuevasPersonas);
  };

  // Renderizar la interfaz del componente
  return (
    <div className="contenedor-tabla">
      <h2>Registro de Personas</h2>
      <table className="tabla-estudiantes">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Telefono</th>
            <th>Habitacion</th>
            <th>Rol</th>
            <th>Contraseña</th>
            <th>Opción</th>
          </tr>
        </thead>
        <tbody>
          {personas.map((persona) => (
            <tr key={persona.email}>
              <td>{persona.username}</td>
              <td>{persona.email}</td>
              <td>{persona.telefono}</td>
              <td>{persona.habitacion}</td>
              <td>{persona.rol}</td>
              <td>{persona.password}</td>
              <td>
                <button className="admin-boton" onClick={() => handleEditarPersona(persona)}>Editar</button>
                <button className="admin-boton" onClick={() => handleEliminarPersona(persona.email)}>Eliminar</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      <div className="registro-admin-boton">
        <button type="button" className="btn" onClick={handleAgregarPersona}>
          Agregar Estudiante
        </button>
      </div>

      {formularioVisible && (
        <form id="registro-estudiante">
          <h2>{personaEditando ? 'Editar' : 'Registrar'} Usuario</h2>
          <label htmlFor="nombre">Nombre completo</label>
          <input
            type="text"
            id="nombre"
            name="nombre"
            value={username}
            onChange={(e) => setNombre(e.target.value)}
            required
          />
          <label htmlFor="email">Correo electrónico</label>
          <input
            type="email"
            id="email"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <label htmlFor="telefono">Teléfono</label>
          <input
            type="tel"
            id="telefono"
            name="telefono"
            value={telefono}
            onChange={(e) => setTelefono(e.target.value)}
            required
            maxLength="10"
          />
          <label htmlFor="contrasena">Contraseña</label>
          <input
            type="password"
            id="contrasena"
            name="contrasena"
            value={password}
            onChange={(e) => setContrasena(e.target.value)}
            required
          />
          <label htmlFor="tipo-habitacion">Habitación asignada</label>
          <select
            id="tipo-habitacion"
            name="habitacion"
            value={habitacion}
            onChange={(e) => setHabitacion(e.target.value)}
            required
          >
            <option value="">Seleccione una habitación</option>
            {habitaciones.map((habitacion) => (
              <option key={habitacion.numero} value={habitacion.numero}>
                {habitacion.numero}
              </option>
            ))}
          </select>
          <label htmlFor="rol">Rol</label>
          <select
            id="rol"
            name="rol"
            value={rol}
            onChange={(e) => setRol(e.target.value)}
            required
          >
            <option value="">Seleccione un rol</option>
            <option value="Estudiante">Estudiante</option>
            <option value="Administrador">Administrador</option>
          </select>

          <button type="button" className="btn" onClick={handleGuardarPersona}>
            Guardar
          </button>
          <button type="button" className="btn" onClick={() => setFormularioVisible(false)}>
            Cancelar
          </button>
        </form>
      )}
    </div>
  );
};

export default RegistroPersonas;
