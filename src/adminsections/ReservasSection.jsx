import React, { useState, useEffect } from 'react';


const ReservasSection = () => {
  const [reservas, setReservas] = useState(() => {

    const storedReservas = JSON.parse(localStorage.getItem('reservas')) || [];
    return storedReservas;
  });

  useEffect(() => {
    localStorage.setItem('reservas', JSON.stringify(reservas));
  }, [reservas]);

  const handleSubmit = (event) => {
    event.preventDefault();

    // Simulación de la reserva
    const nuevaReserva = {
      tipoHabitacion: event.target['tipo-habitacion'].value,
      fechaEntrada: event.target['fecha-entrada'].value,
      fechaSalida: event.target['fecha-salida'].value,
      nombre: event.target['nombre'].value,
      correo: event.target['correo'].value,
      telefono: event.target['telefono'].value,
      metodoPago: event.target['metodo-pago'].value,
      estado: 'Pendiente', // Estado inicial de la reserva
    };

    setReservas([...reservas, nuevaReserva]);
  };
  const handleAceptarReserva = (index) => {
    // Simulación de actualización de estado de reserva
    const nuevasReservas = [...reservas];
    const reservaAceptada = nuevasReservas[index];
  
    // Verificar que la reserva esté en estado 'Pendiente'
    if (reservaAceptada.estado !== 'Pendiente') {
      alert('Esta reserva ya fue procesada.');
      return;
    }
  
    // Obtener la lista actual de usuarios
    const userList = JSON.parse(localStorage.getItem('userList')) || [];
  
    // Buscar el usuario correspondiente en la lista
    const usuarioIndex = userList.findIndex(
      (usuario) => usuario.email === reservaAceptada.correo
    );
  
    // Verificar si se encontró el usuario
    if (usuarioIndex !== -1) {
      // Obtener la lista actual de habitaciones
      const habitacionesList = JSON.parse(localStorage.getItem('habitaciones')) || [];
  
      // Filtrar las habitaciones disponibles del mismo tipo
      const habitacionesDisponibles = habitacionesList.filter(
        (habitacion) => habitacion.tipo === reservaAceptada.tipoHabitacion
      );
  
      // Verificar si hay habitaciones disponibles del mismo tipo
        // Seleccionar aleatoriamente una habitación disponible del mismo tipo
        const habitacionAleatoria = habitacionesDisponibles[Math.floor(Math.random() * habitacionesDisponibles.length)];
  
        // Asignar aleatoriamente un número de habitación al usuario
        userList[usuarioIndex].habitacion = habitacionAleatoria.numero;
    
        // Actualizar el estado en localStorage
        localStorage.setItem('userList', JSON.stringify(userList));
        localStorage.setItem('habitaciones', JSON.stringify(habitacionesList));
  
        // Actualizar el estado en React (si es necesario)
        // setPersonas(userList);
  
        // Actualizar el estado de la reserva a 'Aceptada'
        reservaAceptada.estado = 'Aceptada';
        setReservas(nuevasReservas);
    } else {
      // Manejar caso en el que no se encontró el usuario correspondiente a la reserva
      alert('No se encontró el usuario correspondiente a la reserva.');
    }
  };
  

  const handleDenegarReserva = (index) => {
    // Simulación de actualización de estado de reserva
    const nuevasReservas = [...reservas];
    nuevasReservas[index].estado = 'Denegada';
    setReservas(nuevasReservas);
  };

  return (
    <div className="contenedor-tabla">
      <div className="reservas-lista">
        <h2>Solicitudes de Reservas</h2>
        <table className="reservas-tabla">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Fecha de entrada</th>
              <th>Fecha de salida</th>
              <th>Método de pago</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {reservas.map((reserva, index) => (
              <tr key={index}>
                <td>{reserva.nombre}</td>
                <td>{reserva.fechaEntrada}</td>
                <td>{reserva.fechaSalida}</td>
                <td>{reserva.metodoPago}</td>
                <td>{reserva.estado}</td>
                {reserva.estado === 'Pendiente' && (
                  <td>
                    <button className="admin-boton" onClick={() => handleAceptarReserva(index)}>Aceptar</button>
                    <button className="admin-boton" onClick={() => handleDenegarReserva(index)}>Denegar</button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ReservasSection;
