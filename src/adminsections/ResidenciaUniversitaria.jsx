import React, { useState, useEffect } from 'react';

const ResidenciaUniversitaria = () => {
  const [habitaciones, setHabitaciones] = useState(() => {
    const storedHabitaciones = JSON.parse(localStorage.getItem('habitaciones')) || [];

    if (storedHabitaciones.length === 0) {
      const nuevaHabitacionIndividual = {
        numero: 101,
        tipo: 'Individual',
        precio: 500,
        imagen: 'https://i.ibb.co/qN0Sj5y/individual.jpg',
        caracteristicas: [
          'Cama individual',
          'Espacio de almacenamiento',
          'Escritorio y silla',
          'Baño compartido',
          'Cupos Disponibles: 10/100',
        ],
      };

      const nuevaHabitacionDoble = {
        numero: 104,
        tipo: 'Doble',
        precio: 800,
        imagen: 'https://i.ibb.co/QYh2pPr/doble.jpg',
        caracteristicas: [
          'Camas individuales',
          'Espacio de almacenamiento',
          'Escritorio y sillas',
          'Baño compartido',
          'Cupos Disponibles: 50/100',
        ],
      };

      const nuevaHabitacionPremium = {
        numero: 105,
        tipo: 'Suite',
        precio: 1200,
        imagen: 'https://i.ibb.co/5kKcXL2/suite.jpg',
        caracteristicas: [
          'Cama queen size',
          'Sala de estar privada',
          'Escritorio y silla',
          'Baño privado',
          'Cupos Disponibles: 5/100',
        ],
      };

      return [nuevaHabitacionIndividual, nuevaHabitacionDoble, nuevaHabitacionPremium];
    }

    return storedHabitaciones;
  });

  const [formularioVisible, setFormularioVisible] = useState(false);
  const [tipo, setTipo] = useState('');
  const [precio, setPrecio] = useState('');
  const [caracteristicas, setCaracteristicas] = useState([]);
  const [habitacionEditando, setHabitacionEditando] = useState(null);
  const [imagen, setImagen] = useState(null);

  useEffect(() => {
    localStorage.setItem('habitaciones', JSON.stringify(habitaciones));
  }, [habitaciones]);

  const handleAgregarHabitacion = () => {
    setFormularioVisible(true);
    setHabitacionEditando(null);
    setTipo('');
    setPrecio('');
    setImagen('');
    setCaracteristicas([]);
  };

  const handleGuardarHabitacion = () => {
    if (!tipo || !precio || caracteristicas.length === 0) {
      alert('Por favor, completa todos los campos.');
      return;
    }

    if (habitacionEditando) {
      const nuevasHabitaciones = habitaciones.map((habitacion) =>
        habitacion.numero === habitacionEditando.numero
          ? { ...habitacion, tipo, precio, imagen, caracteristicas }
          : habitacion
      );
      setHabitaciones(nuevasHabitaciones);
    } else {
      const nuevaHabitacion = {
        numero: 100 + habitaciones.length + 15,
        tipo,
        precio: parseInt(precio),
        imagen,
        caracteristicas,
      };
      setHabitaciones([...habitaciones, nuevaHabitacion]);
    }

    setFormularioVisible(false);
    setTipo('');
    setPrecio('');
    setImagen('');
    setCaracteristicas([]);
    setHabitacionEditando(null);
  };

  const handleEditarHabitacion = (habitacion) => {
    setFormularioVisible(true);
    setHabitacionEditando(habitacion);
    setTipo(habitacion.tipo);
    setPrecio(String(habitacion.precio));
    setImagen(habitacion.imagen);
    setCaracteristicas(habitacion.caracteristicas);
  };

  const handleEliminarHabitacion = (numero) => {
    const nuevasHabitaciones = habitaciones.filter((habitacion) => habitacion.numero !== numero);
    setHabitaciones(nuevasHabitaciones);
  };

  return (
    <div className="contenedor-tabla">
      <h1>Gestión de Residencia Universitaria</h1>
      <main>
        <h2>Registro de Habitaciones</h2>
        <table id="tabla-habitaciones" className="tabla-habitaciones">
          <thead>
            <tr>
              <th>Número</th>
              <th>Tipo</th>
              <th>Precio</th>
              <th>Imagen</th>
              <th>Características</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {habitaciones.map((habitacion) => (
              <tr key={habitacion.numero}>
                <td>{habitacion.numero}</td>
                <td>{habitacion.tipo}</td>
                <td>${habitacion.precio}</td>
                <td>{habitacion.imagen}</td>
                <td>
                  <ul>
                    {habitacion.caracteristicas.map((caracteristica, index) => (
                      <li key={index}>{caracteristica}</li>
                    ))}
                  </ul>
                </td>
                <td>
                  <button className="admin-boton" onClick={() => handleEditarHabitacion(habitacion)}>Editar</button>
                  <button className="admin-boton" onClick={() => handleEliminarHabitacion(habitacion.numero)}>Eliminar</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>

        <div className="botones-habitaciones">
          <button type="button" className="btn" onClick={handleAgregarHabitacion}>
            Agregar habitación
          </button>

          {formularioVisible && (
            <div id="formulario">
              <h3>{habitacionEditando ? 'Editar' : 'Agregar'} Habitación</h3>
              <form>
                <label>
                  Tipo:
                  <input
                    type="text"
                    value={tipo}
                    onChange={(e) => setTipo(e.target.value)}
                  />
                </label>
                <label>
                  Precio:
                  <input
                    type="text"
                    value={precio}
                    onChange={(e) => setPrecio(e.target.value)}
                  />
                </label>
                <label>
                  Imagen:
                  <input
                    type="text"
                    value={imagen}
                    onChange={(e) => setImagen(e.target.value)}
                  />
                </label>
                <label>
                  Características (separadas por coma):
                  <input
                    type="text"
                    value={caracteristicas.join(',')}
                    onChange={(e) => setCaracteristicas(e.target.value.split(','))}
                  />
                </label>
                <button type="button" className="btn" onClick={handleGuardarHabitacion}>
                  Guardar
                </button>
                <button type="button" className="btn" onClick={() => setFormularioVisible(false)}>
            Cancelar
          </button>
              </form>
            </div>
          )}
        </div>
      </main>
    </div>
  );
};

export default ResidenciaUniversitaria;
