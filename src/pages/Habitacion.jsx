import React, { useState, useRef } from 'react';
import '../css/habitacion.css'; // Asegúrate de que la ruta al archivo CSS sea correcta

// Definir el componente funcional Administrador
export const Habitacion = (props) => {

  // Obtener datos de habitaciones desde el local storage
  const habitacionesLocalStorage = localStorage.getItem('habitaciones');
  const habitaciones = JSON.parse(habitacionesLocalStorage);
  const habitacionContainerRef = useRef();

  const mostrarImagen = (imagen) => {
    document.body.classList.add("overflow-hidden");
  };

   // Renderizar la interfaz del componente
  return (
    <div>
       <header>
			<div class="logo">
            <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />
			  <p class="uleam-text">ULEAM</p>
			</div>
            <div class="auth-buttons">
              <button class="btn" onClick={() => props.onFormSwitch('index')}>Regresar</button>
              </div>
		</header>
 <div className="contendores-principales">
      <div className="habitacion-todo">
        <h1>Habitaciones disponibles</h1>
        <div className="habitaciones-cuadricula">
          {habitaciones && habitaciones.length > 0 ? (
            habitaciones.map((habitacion, index) => (
                <section
                className={`room ${habitacion.tipo}`}
                key={index}
                ref={habitacionContainerRef}
                onClick={() => mostrarImagen(habitacion.imagen)}
              >
                
                <h2>{habitacion.tipo}</h2>
                <img
                  src={habitacion.imagen}
                  alt={`Imagen de ${habitacion.tipo}`}
                  className="imagen-habitacion"
                  width="300" height="200"
                />
                <p>Precio: {habitacion.precio} por mes</p>
                <p>Características:</p>
                <ul>
                  {habitacion.caracteristicas.map((caracteristica, idx) => (
                    <li key={idx}>{caracteristica.trim()}</li>
                  ))}
                </ul>
                <p>Cupos Disponibles: {habitacion.cupos}</p>
                <button
                  type="button"
                  className="btn"
                  onClick={() => props.onFormSwitch('reservas')}
                >
                  Reservar
                </button>
              </section>
            ))
          ) : (
              
              <div className="no-disponible">
              <h4>No se encontraron habitaciones disponibles </h4>
              </div>
          )}
        </div>
      </div>
    </div>
    <footer className="abajo">
      <strong className="gestion-title">MANTA - MANABÍ - ECUADOR Copyright 2023</strong>
    </footer>
    </div>
  );
};

// Exportar el componente Habitacion
export default Habitacion;