import React from "react";
import '../css/index.css'; // Asegúrate de que la ruta al archivo CSS sea correcta

// Definir el componente funcional Index
export const Index = (props) => {

  // Renderizar la interfaz del componente
  return (
    <div>
      <header>
        <div className="logo">
        <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />
          <p className="uleam-text">ULEAM</p>
        </div>
        <div class="auth-buttons">
        <button class="login" onClick={() => props.onFormSwitch('login')}>Iniciar Sesión</button>
                <button class="register" onClick={() => props.onFormSwitch('register')}>Registrate</button>   
      </div>
      </header>

      <section id="principal">
        <div class="content">
            <div class="left">
                <div class="gestion">
                <strong class="gestion-title">GESTIÓN DE RESIDENCIA UNIVERSITARIA</strong>
                </div>
                <p class="description">
                    Somos un grupo comprometido con la gestión de residencias universitarias en la ULEAM. 
                    Nuestra principal misión es proporcionar un ambiente seguro y cómodo para los estudiantes 
                    de nuestra universidad. Surgimos como respuesta a la necesidad de brindar soluciones de 
                    alojamiento a nuestros compañeros estudiantes, permitiéndoles centrarse en sus estudios 
                    sin preocupaciones adicionales.                 
                </p>
            </div>
            <div class="right">
                <img src={process.env.PUBLIC_URL + "universas.jpeg"} alt="Logo" class="logo-img animated"/>
            </div>
        </div>
    </section>

    <section id="objetivos" class="fondo-obje">
   <div class="content objetivos-list">
              <div class="objetivo-description">
                <h2 class="titulo">Objetivos de Gestión de Residencia</h2>
                <p class="parrafo">Beneficiar a las nuevas generaciones activando los espacios utilizados por ellos, como parques, playas, canchas deportivas, involucrando a los jóvenes del sector a que se sumen al cambio por una nueva generación</p>
                <ul class="services">
                    <li class="service">Limpieza de parques</li>
                    <li class="service">Limpieza de playas</li>
                    <li class="service">Regeneración de canchas deportivas</li>
                </ul>
              </div>
          </div>
            <div class="imagen-contendor">
              <img src={process.env.PUBLIC_URL + "residencias.jpg"} alt="image" class="logo-img animated"/>
          </div>
     </section>
     
      {/* Pie de página */}
      <footer className="footer">
        <strong className="gestion-title">MANTA - MANABÍ - ECUADOR Copyright 2023</strong>
      </footer>
    </div>
  )
};
