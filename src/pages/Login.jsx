import React from "react";
import '../css/login.css'; // Asegúrate de que la ruta al archivo CSS sea correcta

export const Login = (props) => {
  const iniciarSesion = (event) => {
    event.preventDefault();

    // Obtener los valores ingresados en los campos de entrada
    const usernameInput = document.querySelector('#login-username');
    const passwordInput = document.querySelector('#login-password');

    // Obtener la lista de usuarios registrados desde localStorage
    const userList = JSON.parse(localStorage.getItem('userList')) || [];

    if (userList.length === 0) {
      alert('No hay usuarios registrados. Regístrese primero.');
      return false;
    }

    // Realizar la búsqueda en la lista de usuarios registrados
    const foundUser = userList.find((user) => user.username === usernameInput.value && user.password === passwordInput.value);

    if (foundUser) {
      alert('¡Sesión iniciada correctamente!');
      props.onFormSwitch('habitacion');
    } else {
      alert('Credenciales incorrectas. Por favor, inténtelo de nuevo.');
    }
  };


  return (
    <div>
      <header>
        <div className="logo">
        <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />
          <p className="uleam-text">ULEAM</p>
        </div>
        <div class="auth-buttons">
        <button class="login" onClick={() => props.onFormSwitch('adminlogin')}>Administrador</button>
      </div>
      </header>

      <div className="fondo">
        {/* Cuerpo del formulario */}
        <form className="form-login" onSubmit={iniciarSesion}>
          <h2>Iniciar sesión</h2>

          <div className="separador">
            <input required type="text" id="login-username" name="login-username" className="input" />
            <label>
              <span style={{ transitionDelay: '0ms', left: '0px' }}>U</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>s</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>u</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>a</span>
              <span style={{ transitionDelay: '300ms', left: '60px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '72px' }}>i</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Usuario</p>
            </label>
          </div>
          <div className="separador">
            <input required type="password" id="login-password" name="login-password" className="input" />
            <label>
              <span style={{ transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>n</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>t</span>
              <span style={{ transitionDelay: '300ms', left: '56px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '68px' }}>a</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>s</span>
              <span style={{ transitionDelay: '525ms', left: '100px' }}>e</span>
              <span style={{ transitionDelay: '600ms', left: '115px' }}>ñ</span>
              <span style={{ transitionDelay: '712ms', left: '135px' }}>a</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Contraseña</p>
            </label>
          </div>
          
          <span className="link-text">¿No tienes una cuenta?{' '}
          <span className="link-clic" onClick={() => props.onFormSwitch('register')}>Regístrate aquí</span>.</span>
                  <button type="submit" tabIndex="3" className="btn">Iniciar sesión</button>
        </form>
      </div>

      {/* Pie de página */}
      <footer className="footer">
        <strong className="gestion-title">MANTA - MANABÍ - ECUADOR Copyright 2023</strong>
      </footer>
    </div>
  )
};
