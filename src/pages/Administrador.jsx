// Importar React y los componentes necesarios
import React from 'react';
import '../css/administrador.css'; // Asegúrate de que la ruta al archivo CSS sea correcta
import ResidenciaUniversitaria from '../adminsections/ResidenciaUniversitaria';
import RegistroPersonas from '../adminsections/RegistroPersonas';
import ReservasSection from '../adminsections/ReservasSection';


export const Administrador = (props) => {

  return (
      <div>
    <header>
			<div class="logo">
            <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />
			  <p class="uleam-text">ULEAM</p>
			</div>
            <div class="auth-buttons">
              <button class="btn" onClick={() => props.onFormSwitch('index')}>Regresar</button>
              </div>
		</header>

        <ResidenciaUniversitaria />
        <RegistroPersonas />
        <ReservasSection />

        

 </div>

  );
};
