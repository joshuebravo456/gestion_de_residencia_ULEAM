import React, { useState } from 'react';
import '../css/login.css'; // Asegúrate de que la ruta al archivo CSS sea correcta

export const AdminLogin = (props) => {
   // Estado para la lista de usuarios
  const [userList, setUserList] = useState(JSON.parse(localStorage.getItem("userList")) || []);

  // Estado para el mensaje de error
  const [errorMessage, setErrorMessage] = useState('');

  // Verificar si el usuario administrador ya existe en la lista
  const adminExists = userList.some(user => user.rol === 'Administrador');

  // Agregar un usuario administrador por defecto si no existe en la lista
  if (!adminExists) {
    const usuarioAdministrador = {
      username: "admin",
      email: "admin@example.com",
      telefono: '0',
      password: "admin123",
      habitacion: '0',
      rol: 'Administrador'
    };
    setUserList([...userList, usuarioAdministrador]);
    localStorage.setItem("userList", JSON.stringify([...userList, usuarioAdministrador]));
  }

  // Manejar el envío del formulario
  const handleSubmit = (e) => {
    e.preventDefault();
    // Obtener los valores ingresados en los campos de entrada
    const usernameInput = document.querySelector('#username');
    const passwordInput = document.querySelector('#password');
    
    const user = userList.find((user) => user.username === usernameInput.value && user.password === passwordInput.value);
    
    if (user) {
      if (user.rol === "Administrador") {
        // Si el usuario tiene el rol de administrador, redirigir a la página de administrador
        alert("¡Sesión iniciada correctamente como administrador!");
        props.onFormSwitch('administrador');
        // Aquí puedes redirigir a la página de administrador usando el enrutador de React
      } else {
        // Si el usuario no tiene el rol de administrador, mostrar un mensaje de acceso denegado
        setErrorMessage("Acceso denegado. No tienes permiso para acceder a esta página.");
      }
    } else {
      // Si no se encuentra el usuario en la lista, mostrar un mensaje de credenciales incorrectas
      setErrorMessage("Credenciales incorrectas. Por favor, intenta de nuevo.");
    }
  };



  return (
    <div>
      <header>
        <div className="logo">
        <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />
          <p className="uleam-text">ULEAM</p>
        </div>
      </header>

      <div className="fondo">
        {/* Cuerpo del formulario */}
        
        <form className="form-login" onSubmit={handleSubmit}>
          <h2>Administrador</h2>

          <div className="separador">
          <input required type="text" id="username" name="username" className="input" />
            <label>
              <span style={{ transitionDelay: '0ms', left: '0px' }}>U</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>s</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>u</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>a</span>
              <span style={{ transitionDelay: '300ms', left: '60px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '72px' }}>i</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Usuario</p>
            </label>
          </div>
          <div className="separador">
          <input required type="password" id="password" name="password" className="input" />
            <label>
              <span style={{ transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>n</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>t</span>
              <span style={{ transitionDelay: '300ms', left: '56px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '68px' }}>a</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>s</span>
              <span style={{ transitionDelay: '525ms', left: '100px' }}>e</span>
              <span style={{ transitionDelay: '600ms', left: '115px' }}>ñ</span>
              <span style={{ transitionDelay: '712ms', left: '135px' }}>a</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Contraseña</p>
            </label>
          </div>
          <button type="submit" tabIndex="3" className="btn">Iniciar sesión</button>
          {errorMessage && <p>{errorMessage}</p>}
          </form>
      </div>

      {/* Pie de página */}
      <footer className="footer">
        <strong className="gestion-title">MANTA - MANABÍ - ECUADOR Copyright 2023</strong>
      </footer>
    </div>
  )
};
