import React, { useState, useEffect } from 'react';
import '../css/reservar.css'; // Asegúrate de que la ruta al archivo CSS sea correcta

// Año actual para limitar las fechas de reserva
const currentYear = new Date().getUTCFullYear();

// Componente funcional Reservas
export const Reservas = (props) => {
  // Estados para manejar la información del formulario de reserva
  const [tipoHabitacion, setTipoHabitacion] = useState('individual');
  const [fechaEntrada, setFechaEntrada] = useState('');
  const [fechaSalida, setFechaSalida] = useState('');
  const [nombre, setNombre] = useState('');
  const [correo, setCorreo] = useState('');
  const [telefono, setTelefono] = useState('');
  const [metodoPago, setMetodoPago] = useState('');
  const [numeroTarjeta, setNumeroTarjeta] = useState('');
  const [nombreTarjeta, setNombreTarjeta] = useState('');
  const [fechaVencimiento, setFechaVencimiento] = useState('');
  const [codigoSeguridad, setCodigoSeguridad] = useState('');
  const [banco, setBanco] = useState('');
  const [cuenta, setCuenta] = useState('');
  const [correoPaypal, setCorreoPaypal] = useState('');
  const [reservas, setReservas] = useState([]);
  const [habitaciones, setHabitaciones] = useState([]);
  // ... (otros estados)

  // Efecto para cargar las habitaciones desde el almacenamiento local al montar el componente
  useEffect(() => {
    const storedHabitaciones = JSON.parse(localStorage.getItem('habitaciones')) || [];
    setHabitaciones(storedHabitaciones);
  }, []); 

  // Efecto para cargar las reservas desde el almacenamiento local al montar el componente
  useEffect(() => {
    const storedReservas = JSON.parse(localStorage.getItem('reservas')) || [];
    setReservas(storedReservas);
  }, []); 

  // Función para formatear y manejar cambios en el número de tarjeta
  const handleChangeNumeroTarjeta = (e) => {
    let inputNumber = e.target.value.replace(/\D/g, ''); // Eliminar caracteres no numéricos
    inputNumber = inputNumber.slice(0, 16); // Limitar a 16 dígitos
    let formattedNumber = '';
  
    for (let i = 0; i < inputNumber.length; i++) {
      if (i > 0 && i % 4 === 0) {
        formattedNumber += ' '; // Agregar un espacio después de cada grupo de 4 dígitos
      }
      formattedNumber += inputNumber[i];
    }
  
    setNumeroTarjeta(formattedNumber);
  };  
 // Función para manejar cambios en el código de seguridad
  const handleChangeCodigoSeguridad = (e) => {
    const securityCode = e.target.value.slice(0, 3); // Limitar a 3 dígitos
    setCodigoSeguridad(securityCode);
  };
  // Función para manejar la presentación del formulario al enviar
  const handleSubmit = (e) => {
    e.preventDefault();

      // Validar que fechaSalida sea mayor que fechaEntrada
  const entradaDate = new Date(fechaEntrada);
  const salidaDate = new Date(fechaSalida);

  if (salidaDate <= entradaDate) {
    alert("La fecha de salida debe ser posterior a la fecha de entrada.");
    return;
  }

  // Validar que ambas fechas sean del mismo año
  if (entradaDate.getFullYear() !== salidaDate.getFullYear()) {
    alert("Las fechas deben pertenecer al mismo año.");
    return;
  }

    // Validar el formato del correo electrónico
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(correo) && metodoPago !== 'paypal') {
    alert("Por favor, ingrese un correo electrónico válido.");
    return;
  }

  // Validar el formato del número de teléfono
  const phoneRegex = /^\d{10}$/;
  if (!phoneRegex.test(telefono)) {
    alert("Por favor, ingrese un número de teléfono válido.");
    return;
  }
  const cardNumberRegex = /^\d{4}[- ]?\d{4}[- ]?\d{4}[- ]?\d{4}$/;
  if (!cardNumberRegex.test(numeroTarjeta)) {
    alert("Por favor, ingrese un número de tarjeta válido (16 dígitos en grupos de 4).");
    return;
  }

  // Validar el formato del código de seguridad (3 dígitos)
  const securityCodeRegex = /^\d{3}$/;
  if (!securityCodeRegex.test(codigoSeguridad)) {
    alert("Por favor, ingrese un código de seguridad válido (3 dígitos).");
    return;
  }
    // Crear un objeto con la nueva reserva
    const nuevaReserva = {
      tipoHabitacion,
      fechaEntrada,
      fechaSalida,
      nombre,
      correo,
      telefono,
      metodoPago,
      numeroTarjeta,
      nombreTarjeta,
      fechaVencimiento,
      codigoSeguridad,
      banco,
      cuenta,
      correoPaypal,
      estado: 'Pendiente',
    };
  
    // Actualizar el estado con la nueva reserva
    setReservas([...reservas, nuevaReserva]);
  
    // Limpiar los campos después de agregar la reserva
    setTipoHabitacion('individual');
    setFechaEntrada('');
    setFechaSalida('');
    setNombre('');
    setCorreo('');
    setTelefono('');
    setMetodoPago('');
    setNumeroTarjeta('');
    setNombreTarjeta('');
    setFechaVencimiento('');
    setCodigoSeguridad('');
    setBanco('');
    setCuenta('');
    setCorreoPaypal('');
  
    // Guardar las reservas actualizadas en localStorage
    localStorage.setItem('reservas', JSON.stringify([...reservas, nuevaReserva]));
  
    alert("Reserva registrada correctamente");
  };
  
  // Función para renderizar los campos específicos del método de pago seleccionado
  const handleMetodoPagoChange = (e) => {
    const selectedMetodoPago = e.target.value;
    setMetodoPago(selectedMetodoPago);

    // Limpiar los campos de otros métodos de pago al cambiar la selección
    setNumeroTarjeta('');
    setNombreTarjeta('');
    setFechaVencimiento('');
    setCodigoSeguridad('');
    setBanco('');
    setCuenta('');
    setCorreoPaypal('');
  };
  const renderMetodoPagoFields = () => {
    const commonProps = { className: "formulario-reservar", required: true };
  
    switch (metodoPago) {
      case 'tarjeta':
        return (
          <div id="tarjeta" className="formulario-container">
            <div className="input-container">
              <label htmlFor="numero-tarjeta">Número de tarjeta:</label>
              <input type="text" id="numero-tarjeta" name="numero-tarjeta" value={numeroTarjeta} onChange={handleChangeNumeroTarjeta}{...commonProps} />
            </div>
            <div className="input-container">
              <label htmlFor="fecha-vencimiento">Fecha de vencimiento:</label>
              <input type="month" id="fecha-vencimiento" name="fecha-vencimiento" value={fechaVencimiento} onChange={(e) => setFechaVencimiento(e.target.value)} {...commonProps} />
            </div>
            <div className="input-container">
              <label htmlFor="codigo-seguridad">Código de seguridad:</label>
              <input type="text" id="codigo-seguridad" name="codigo-seguridad" value={codigoSeguridad} onChange={handleChangeCodigoSeguridad} {...commonProps} />
            </div>
          </div>
        );
      case 'transferencia':
        return (
          <div id="transferencia" className="formulario-container">
            <div className="input-container">
              <label htmlFor="banco">Banco:</label>
              <input type="text" id="banco" name="banco" value={banco} onChange={(e) => setBanco(e.target.value)} {...commonProps} />
            </div>
            <div className="input-container">
              <label htmlFor="cuenta">Número de cuenta:</label>
              <input type="text" id="cuenta" name="cuenta" value={cuenta} onChange={(e) => setCuenta(e.target.value)} {...commonProps} />
            </div>
          </div>
        );
      case 'paypal':
        return (
          <div id="paypal" className="formulario-container">
            <div className="input-container">
              <label htmlFor="correo-paypal">Correo electrónico de PayPal:</label>
              <input type="email" id="correo-paypal" name="correo-paypal" value={correoPaypal} onChange={(e) => setCorreoPaypal(e.target.value)} {...commonProps} />
            </div>
          </div>
        );
      default:
        return null;
    }
  };
  return (
    <div>
        <header>
        <div className="logo">
        <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />
          <p className="uleam-text">ULEAM</p>
        </div>
        <div class="auth-buttons">
        <button class="login" onClick={() => props.onFormSwitch('habitacion')}>Regresar</button>
      </div>
      </header>
      <div class="fondo">
      <div class="reservas-contenedor">
      <div className="reservasdatos-contenedor">
      <h1>Reserva tu habitación</h1>

      <form class="reserva-form" id="reserva-form" onSubmit={handleSubmit}>
        
      <div class="columna-izquierda">
      <div className="formulario-reservar">
            <label htmlFor="tipo-habitacion">Tipo de habitación:</label>
            <select
            id="tipo-habitacion"
            name="tipo-habitacion"
            value={tipoHabitacion}
            onChange={(e) => {
              setTipoHabitacion(e.target.value);
              console.log("Tipo de habitación seleccionado:", e.target.value);
            }}
          >
            {habitaciones.map((habitacion, index) => (
              <option key={index} value={habitacion.tipo}>
                {habitacion.tipo}
              </option>
            ))}
          </select>
          </div>
          <div className="formulario-reservar">
            <label htmlFor="fecha-entrada">Fecha de entrada:</label>
            <input type="date" id="fecha-entrada" name="fecha-entrada" required value={fechaEntrada} onChange={(e) => setFechaEntrada(e.target.value)} max={`${currentYear}-12-31`} />
          </div>
          <div className="formulario-reservar">
            <label htmlFor="fecha-salida">Fecha de salida:</label>
            <input type="date" id="fecha-salida" name="fecha-salida" required value={fechaSalida} onChange={(e) => setFechaSalida(e.target.value)} max={`${currentYear}-12-31`} />
          </div>
          <div className="formulario-reservar">
            <label htmlFor="nombre">Nombre completo:</label>
            <input type="text" id="nombre" name="nombre" required value={nombre} onChange={(e) => setNombre(e.target.value)} />
          </div>
          <div className="formulario-reservar">
            <label htmlFor="correo">Correo electrónico:</label>
            <input type="email" id="correo" name="correo" required value={correo} onChange={(e) => setCorreo(e.target.value)} />
          </div>
          <div className="formulario-reservar">
            <label htmlFor="telefono">Teléfono:</label>
            <input type="tel" id="telefono" name="telefono" required pattern="[0-9]+" value={telefono} onChange={(e) => setTelefono(e.target.value)} />
          </div>
          </div>

          <div class="columna-derecha">
          <div className="formulario-reservar">
              <label htmlFor="metodo-pago">Método de pago:</label>
              <select id="metodo-pago" name="metodo-pago" required value={metodoPago} onChange={handleMetodoPagoChange}>
                <option value="">Seleccione un método de pago</option>
                <option value="tarjeta">Tarjeta de crédito/débito</option>
                <option value="transferencia">Transferencia bancaria</option>
                <option value="paypal">PayPal</option>
              </select>
            </div>
      {renderMetodoPagoFields()}
      <button type="submit" className="btn">Reservar</button>

      </div>
      </form>
      </div>
    </div>
    </div>
    <footer className="footer">
        <strong className="gestion-title">MANTA - MANABÍ - ECUADOR Copyright 2023</strong>
      </footer>
    </div>
  );
};
