import React from 'react';
import '../css/registro.css'; // Asegúrate de que la ruta al archivo CSS sea correcta

export const Register = (props) => {
 // Función para manejar el registro
 const registrar = (event) => {
  event.preventDefault();

  // Obtener referencias a los elementos de entrada del formulario
  const usernameInput = document.querySelector('#username'); // Input del nombre de usuario
  const emailInput = document.querySelector('#email');      // Input del nombre de usuario
  const passwordInput = document.querySelector('#password'); // Input de la contraseña
  const confirmPasswordInput = document.querySelector('#confirm-password');// Input para confirmar la contraseña
  const telefonoInput = document.querySelector('#telefono');// Input del número de teléfono

  
  const emailFormat = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;// Evitar el envío del formulario
  const phonePattern = /^\d{10}$/; // Cambiado para requerir 10 dígitos
  const passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_]).{8,}$/; // Nueva expresión regular para la contraseña
  
  // Verificar si alguno de los campos obligatorios está vacío
  if (usernameInput.value === '' || emailInput.value === '' || passwordInput.value === '' || confirmPasswordInput.value === '') { 
    alert('Por favor, complete todos los campos.'); // Mostrar mensaje de alerta si falta algún campo
    return false; // Evitar el envío del formulario
  }

  if (!emailFormat.test(emailInput.value)) {
    alert('El correo electrónico no tiene un formato válido.'); // Mostrar mensaje de alerta si el formato del correo es inválido
    return false; // Evitar el envío del formulario
  }

  if (!phonePattern.test(telefonoInput.value)) {
    alert('El número celular debe contener exactamente 10 dígitos.');// Mostrar mensaje de alerta si el número de teléfono es inválido
    return false;// Evitar el envío del formulario
  }

  if (!passwordFormat.test(passwordInput.value)) {
    alert('La contraseña debe contener al menos una letra mayúscula, una letra minúscula, un número y un carácter especial. Debe tener una longitud mínima de 8 caracteres.');
    return false;
  }

  if (passwordInput.value !== confirmPasswordInput.value) {
    alert('Las contraseñas no coinciden.');// Mostrar mensaje de alerta si las contraseñas no coinciden
    return false;// Evitar el envío del formulario
  }

  // Obtener la lista actual de usuarios registrados o inicializarla si es la primera vez
  const userList = JSON.parse(localStorage.getItem('userList')) || [];

  // Verificar si el nombre de usuario ya existe
  const usernameExists = userList.some(user => user.username === usernameInput.value);
  if (usernameExists) {
    alert('El nombre de usuario ya está en uso. Por favor, elige otro.');
    return false;
  }

  // Verificar si el correo electrónico ya existe
  const emailExists = userList.some(user => user.email === emailInput.value);
  if (emailExists) {
    alert('El correo electrónico ya está registrado. Por favor, utiliza otro.');
    return false;
  }

  // Agregar el nuevo usuario a la lista
  userList.push({
    username: usernameInput.value,
    email: emailInput.value,
    telefono: telefonoInput.value,
    password: passwordInput.value,
    habitacion: '0',
    rol: 'Estudiante',
  });

  // Almacenar la lista actualizada en localStorage
  localStorage.setItem('userList', JSON.stringify(userList));
  alert('¡Registrado correctamente!');
  props.onFormSwitch('login');
};


  return (
    <div className="cosas" style={{ overflowY: 'auto', maxHeight: '600px' }}>
    <header>
      <div className="logo">
        <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />


        <p className="uleam-text">ULEAM</p>
      </div>
    </header>

    <div className="cosas">
      <form className="register-form" onSubmit={registrar}>
          <h2>REGISTRO</h2>
          <div className="registro-separador">
            <input required type="text" id="username" className="input" />
            <label>
              <span style={{ fontSize: '20px', transitionDelay: '0ms', left: '0px' }}>U</span>
              <span style={{ fontSize: '20px',transitionDelay: '75ms', left: '20px' }}>s</span>
              <span style={{ fontSize: '20px',transitionDelay: '150ms', left: '33px' }}>u</span>
              <span style={{ fontSize: '20px',transitionDelay: '225ms', left: '47px' }}>a</span>
              <span style={{ fontSize: '20px',transitionDelay: '300ms', left: '60px' }}>r</span>
              <span style={{ fontSize: '20px',transitionDelay: '375ms', left: '72px' }}>i</span>
              <span style={{ fontSize: '20px',transitionDelay: '450ms', left: '85px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '20px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Usuario</p>
            </label>
          </div>

          <div className="registro-separador">
            <input required type="text" id="nombres" className="input" />
            <label>
              <span style={{ fontSize: '20px',transitionDelay: '0ms', left: '0px' }}>N</span>
              <span style={{ fontSize: '20px',transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ fontSize: '20px',transitionDelay: '150ms', left: '33px' }}>m</span>
              <span style={{ fontSize: '20px',transitionDelay: '225ms', left: '57px' }}>b</span>
              <span style={{ fontSize: '20px',transitionDelay: '300ms', left: '71px' }}>r</span>
              <span style={{ fontSize: '20px',transitionDelay: '375ms', left: '80px' }}>e</span>
              <span style={{ fontSize: '20px',transitionDelay: '450ms', left: '95px' }}>s</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '20px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Nombres</p>
            </label>
          </div>
          <div className="registro-separador">
            <input required type="text" id="apellidos" className="input" />
            <label>
              <span style={{ fontSize: '20px',transitionDelay: '0ms', left: '0px' }}>A</span>
              <span style={{ fontSize: '20px',transitionDelay: '75ms', left: '20px' }}>p</span>
              <span style={{ fontSize: '20px',transitionDelay: '150ms', left: '33px' }}>e</span>
              <span style={{ fontSize: '20px',transitionDelay: '225ms', left: '47px' }}>l</span>
              <span style={{ fontSize: '20px',transitionDelay: '300ms', left: '56px' }}>l</span>
              <span style={{ fontSize: '20px',transitionDelay: '375ms', left: '68px' }}>i</span>
              <span style={{ fontSize: '20px',transitionDelay: '450ms', left: '85px' }}>d</span>
              <span style={{ fontSize: '20px',transitionDelay: '525ms', left: '100px' }}>o</span>
              <span style={{ fontSize: '20px',transitionDelay: '600ms', left: '115px' }}>s</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '20px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Apellidos</p>
            </label>
          </div>
          <div className="registro-separador">
            <input required type="text" id="telefono" className="input" />
            <label>
              <span style={{ fontSize: '20px',transitionDelay: '0ms', left: '0px' }}>T</span>
              <span style={{ fontSize: '20px',transitionDelay: '75ms', left: '20px' }}>e</span>
              <span style={{ fontSize: '20px',transitionDelay: '150ms', left: '33px' }}>l</span>
              <span style={{ fontSize: '20px',transitionDelay: '225ms', left: '40px' }}>e</span>
              <span style={{ fontSize: '20px',transitionDelay: '300ms', left: '56px' }}>f</span>
              <span style={{ fontSize: '20px',transitionDelay: '375ms', left: '68px' }}>o</span>
              <span style={{ fontSize: '20px',transitionDelay: '450ms', left: '85px' }}>n</span>
              <span style={{ fontSize: '20px',transitionDelay: '525ms', left: '100px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '20px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Telefono</p>
            </label>
          </div>
          <div className="registro-separador">
            <input required type="text" id="email" name="email" className="input" />
            <label>
              <span style={{ fontSize: '20px',transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ fontSize: '20px',transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ fontSize: '20px',transitionDelay: '150ms', left: '34px' }}>r</span>
              <span style={{ fontSize: '20px',transitionDelay: '225ms', left: '43px' }}>r</span>
              <span style={{ fontSize: '20px',transitionDelay: '300ms', left: '50px' }}>e</span>
              <span style={{ fontSize: '20px',transitionDelay: '375ms', left: '65px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '20px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Correo</p>
            </label>
          </div>
          <div className="registro-separador">
            <input required type="password" id="password" name="password" className="input" />
            <label>
              <span style={{ fontSize: '20px',transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ fontSize: '20px',transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ fontSize: '20px',transitionDelay: '150ms', left: '33px' }}>n</span>
              <span style={{ fontSize: '20px',transitionDelay: '225ms', left: '47px' }}>t</span>
              <span style={{ fontSize: '20px',transitionDelay: '300ms', left: '56px' }}>r</span>
              <span style={{ fontSize: '20px',transitionDelay: '375ms', left: '68px' }}>a</span>
              <span style={{ fontSize: '20px',transitionDelay: '450ms', left: '85px' }}>s</span>
              <span style={{ fontSize: '20px',transitionDelay: '525ms', left: '100px' }}>e</span>
              <span style={{ fontSize: '20px',transitionDelay: '600ms', left: '115px' }}>ñ</span>
              <span style={{ fontSize: '20px',transitionDelay: '712ms', left: '135px' }}>a</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '20px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Contraseña</p>
            </label>
          </div>
          <div className="registro-separador">
            <input required type="password" id="confirm-password" name="confirm-password" className="input" />
            <label>
              <span style={{ fontSize: '20px',transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ fontSize: '20px',transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ fontSize: '20px',transitionDelay: '150ms', left: '33px' }}>n</span>
              <span style={{ fontSize: '20px',transitionDelay: '225ms', left: '47px' }}>t</span>
              <span style={{ fontSize: '20px',transitionDelay: '300ms', left: '56px' }}>r</span>
              <span style={{fontSize: '20px', transitionDelay: '375ms', left: '68px' }}>a</span>
              <span style={{fontSize: '20px', transitionDelay: '450ms', left: '85px' }}>s</span>
              <span style={{fontSize: '20px', transitionDelay: '525ms', left: '100px' }}>e</span>
              <span style={{fontSize: '20px', transitionDelay: '600ms', left: '115px' }}>ñ</span>
              <span style={{fontSize: '20px', transitionDelay: '712ms', left: '135px' }}>a</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '20px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Confirmar</p>
            </label>
          </div>
       
          <span className="link-text">¿Ya tienes una cuenta?{' '}
          <span className="link-clic" onClick={() => props.onFormSwitch('login')}>Inicia sesión aquí</span>.</span>
          <button type="submit" tabIndex="6" className="btn">Registrarse</button>
        </form>
      </div>

    <footer className="footer">
      <strong className="gestion-title">MANTA - MANABÍ - ECUADOR Copyright 2023</strong>
    </footer>
  </div>
  );
};
